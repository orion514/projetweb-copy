<?php
/**
 * @file
 * Integrates the 'Add to homescreen' library with Drupal.
 */

/**
 * Implements hook_libraries_info().
 */
function addtohomescreen_libraries_info() {
  $libraries['addtohomescreen'] = array(
    'name' => 'Add to homescreen',
    'vendor url' => 'https://github.com/cubiq/add-to-homescreen',
    'download url' => 'https://github.com/cubiq/add-to-homescreen',
    'version arguments' => array(
      'file' => 'src/addtohomescreen.js',
      'pattern' => '# v([0-9\.]+)#',
      'lines' => 100,
    ),
    'variants' => array(
      'minified' => array(
        'files' => array(
          'css' => array('style/addtohomescreen.css'),
          'js' => array('src/addtohomescreen.min.js'),
        ),
      ),
      'source' => array(
        'files' => array(
          'css' => array('style/addtohomescreen.css'),
          'js' => array('src/addtohomescreen.js'),
        ),
      ),
    ),
    'integration files' => array(
      'addtohomescreen' => array(
        'js' => array('addtohomescreen.js' => array()),
      ),
    ),
    'post-load integration files' => TRUE,
  );

  return $libraries;
}

/**
 * Implements hook_permission().
 */
function addtohomescreen_permission() {
  return array(
    'administer add to homescreen' => array(
      'title' => t('Administer Add to homescreen'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function addtohomescreen_menu() {
  $items['admin/config/user-interface/addtohomescreen'] = array(
    'title' => 'Add to homescreen',
    'description' => 'Configuration for the Add to homescreen library',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('addtohomescreen_settings'),
    'access arguments' => array('administer add to homescreen'),
    'file' => 'addtohomescreen.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_page_build().
 */
function addtohomescreen_page_build(&$page) {
  if (($library = libraries_detect('addtohomescreen')) && !empty($library['installed'])) {
    $attached = &$page['page_bottom']['addtohomescreen']['#attached'];

    $settings = array(
      'debug' => variable_get('addtohomescreen_debug', FALSE),
      'modal' => variable_get('addtohomescreen_modal', FALSE),
      'mandatory' => variable_get('addtohomescreen_mandatory', FALSE),
      'skipFirstVisit' => variable_get('addtohomescreen_skipfirstvisit', TRUE),
      'autostart' => variable_get('addtohomescreen_autostart', TRUE),
      'icon' => variable_get('addtohomescreen_icon', TRUE),
      'startDelay' => (int) variable_get('addtohomescreen_startdelay', 1),
      'lifespan' => (int) variable_get('addtohomescreen_lifespan', 15),
      'displayPace' => (int) variable_get('addtohomescreen_displaypace', 1440),
      'maxDisplayCount' => (int) variable_get('addtohomescreen_maxdisplaycount', 1),
    );
    if (variable_get('addtohomescreen_use_custom_message', FALSE)) {
      $settings['message'] = variable_get('addtohomescreen_message', '');
    }
    $variant = variable_get('addtohomescreen_compression_type', 'minified');
    $attached['libraries_load'][] = array('addtohomescreen', $variant);
    $attached['js'][] = array(
      'data' => array('addtohomescreen' => $settings),
      'type' => 'setting',
    );
  }
}
