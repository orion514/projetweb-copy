/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {
	//
	$(document).ready(function(){
	//
		$(window).load(function(){

          $("#preloader").delay(1000).fadeOut("slow");
            setTimeout(function(){ $('body').css('overflow', 'auto');}, 1000);



				$( "#click_on_it" ).click(function() {
				  $( "#background_div" ).addClass( "black_sheep_wall" );
				  $( "#background_div" ).removeClass( "black_sheep_wall_closed" );
				  $( "#click_on_it" ).addClass( "displaynone" );
				  $( "#click_on_it_2" ).removeClass( "displaynone" );
				});

		});		
		//
		$( "#click_on_it_2" ).click(function() {
			$( "#background_div" ).removeClass( "black_sheep_wall" );
			$( "#background_div" ).addClass( "black_sheep_wall_closed" );
			$( "#click_on_it" ).removeClass( "displaynone" );
			$( "#click_on_it_2" ).addClass( "displaynone" );
		});
	//	Fonctions END
	})
	//
	// To understand behaviors, see https://drupal.org/node/756722#behaviors
	Drupal.behaviors.my_custom_behavior = {
	  attach: function(context, settings) {
		// Formatage du bloc actualite_media


	  }
	};

	
})(jQuery, Drupal, this, this.document);

