﻿<?php
/**
 * @file
 * Returns the HTML for a node.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728164
 */
?>
<img src="https://projet-web.ca/sites/all/themes/bootstrap/css/images/front-banner_commerce_en.jpg" class="img_banner_gallery commerce_en">
<img src="https://projet-web.ca/sites/all/themes/bootstrap/css/images/front-banner_commerce_fr.jpg" class="img_banner_gallery commerce_fr">
<img src="https://projet-web.ca/sites/all/themes/bootstrap/css/images/front-mobile_commerce_en.jpg" class="img_banner_gallery commerce_mobile_en">
<img src="https://projet-web.ca/sites/all/themes/bootstrap/css/images/front-mobile_commerce_fr.jpg" class="img_banner_gallery commerce_mobile_fr">