<?php
/**
 * @file
 * progress-bar.tpl.php
 *
 * Variables
 * - $percent: The percentage of the progress.
 * - $message: A string containing information to be displayed.
 */
?>		
		<div class="responsive-front">
			<article>
				<!-- -------------------------------------------------------------------------- -->				
				<!-- --------------------------  Big Display ---------------------------------- -->	
				<!-- -------------------------------------------------------------------------- -->
				<section class="header_content">
					<img src="https://www.projet-web.ca/sites/all/themes/bootstrap/css/images/front-banner_commerce_details_en.jpg" class="commerce_details_en">
					<img src="https://www.projet-web.ca/sites/all/themes/bootstrap/css/images/front-banner_commerce_details_fr.jpg" class="commerce_details_fr">
					
					<img src="https://projet-web.ca/sites/all/themes/bootstrap/css/images/front-mobile_commerce_en.jpg" class="img_banner_gallery commerce_mobile_en">
					<img src="https://projet-web.ca/sites/all/themes/bootstrap/css/images/front-mobile_commerce_fr.jpg" class="img_banner_gallery commerce_mobile_fr">
					
					<div class="form-type-bef-link form-item form-group store_link_en clearfix">
					 <a href="https://projet-web.ca/en/store">Back</a>
					</div>
					<div class="form-type-bef-link form-item form-group store_link_fr clearfix">
					 <a href="https://projet-web.ca/fr/boutique">Retour</a>
					</div>
				</section>			
				<section class="shop_section slider_big_display">
					<div class="side_b">
						<div class="product_display_details">
							<!--  -->	
							<h2  class="clearfix"><?php print render($content['field_titre_article']['#items']['0']['value']); ?></h2>
							<img src="<?php print render($content['field_t_shirt_image']['0']); ?>">	
							<!--  -->	
							<div class="produit">
							<?php print render($content['field_product']); ?>
							</div>
							<!--  -->
						</div>
					</div>
					<div class="side_a">				
						<div class="div_description">
							<h4>Description</h4>
							<?php print render($content['field_t_shirt_names']); ?>
							<!--  -->	
							<?php print render($content['field_choix_du_sexe']); ?>
							<?php print render($content['field_couleur']); ?>
							<?php print render($content['body']); ?>
							
							<?php print render($content['field_prix']); ?>
						</div>
					</div>
				</section>
				<!-- -------------------------------------------------------------------------- -->				
				<!-- --------------------------  Big Display ---------------------------------- -->	
				<!-- -------------------------------------------------------------------------- -->	
			</article>
		</div>
		
<script type="text/javascript">  
  /* Fonction qui redimensionne les sections*/
 (function($){$(document).ready(function(){
	$(function() {
        // get test settings
        var byRow = $('body').hasClass('test-rows');
        // apply matchHeight to each item container's items
        $('.div_equal').each(function() {
            $(this).children('.equal_this').matchHeight({
            byRow: byRow
            //property: 'min-height'
            });
        });
    });
});
}(jQuery));
</script>
