<?php
/**
 * @file
 * progress-bar.tpl.php
 *
 * Variables
 * - $percent: The percentage of the progress.
 * - $message: A string containing information to be displayed.
 */
?>

<div class="responsive-front">
	<section class="google_map_big">				
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2796.6405336469384!2d-73.435647!3d45.497183!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4cc9045297ea4d7f%3A0xe367379a44ed2d32!2sTony+Rosato!5e0!3m2!1sfr!2sca!4v1426616829168" width="1555" height="200" frameborder="0" style="border:0"></iframe>		
	</section>
	
	<section class="google_map_mobile">				
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2796.6405336469384!2d-73.435647!3d45.497183!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4cc9045297ea4d7f%3A0xe367379a44ed2d32!2sTony+Rosato!5e0!3m2!1sfr!2sca!4v1426616829168" width="750" height="200" frameborder="0" style="border:0"></iframe>		
	</section>
</div>
