<?php
/**
 * @file
 * progress-bar.tpl.php
 *
 * Variables
 * - $percent: The percentage of the progress.
 * - $message: A string containing information to be displayed.
 */
?>		
		<!-- -------------------------------------------------------------------------- -->				
		<!-- --------------------------  Mobile Display ------------------------------- -->	
		<!-- -------------------------------------------------------------------------- -->		
		<div class="responsive-front">
			<article>
				<section class="projets_all slider_small_display">
					<div class="side_a">				
						<img src="<?php print render($content['field_slider_image_mobile']['0']); ?>">
						<?php if (!empty($content['field_titre_section']['#items']['0']['value'])): ?>
						<div class="folio_bg_title">
							<h2><?php print render($content['field_titre_section']['#items']['0']['value']); ?></h2>
							<h4><?php print render($content['field_sous_titre_section_1']['#items']['0']['value']); ?></h4>
						</div>
						<?php endif; ?>
					</div>
					<!-- Deuxième side -->
					<div class="side_b">
						<div class="text_folio">
							<?php if (!empty($content['field_paragraphe_section_1']['#items']['0']['value'])): ?>
							<p><?php print render($content['field_paragraphe_section_1']['#items']['0']['value']); ?></p>
							<?php endif; ?>	
							<?php if (!empty($content['field_paragraphe_section_2']['#items']['0']['value'])): ?>
							<p><?php print render($content['field_paragraphe_section_2']['#items']['0']['value']); ?></p>
							<?php endif; ?>	
						</div>
						<!-- Liste -->
							<ul class="liste_folio">
								<?php if (!empty($content['field_liste_section_1']['#items']['0']['value'])): ?>
								<li><?php print render($content['field_liste_section_1']['#items']['0']['value']); ?></li>
								<?php endif; ?>
								<?php if (!empty($content['field_liste_section_1']['#items']['1']['value'])): ?>
								<li><?php print render($content['field_liste_section_1']['#items']['1']['value']); ?></li>
								<?php endif; ?>
								<?php if (!empty($content['field_liste_section_1']['#items']['2']['value'])): ?>
								<li><?php print render($content['field_liste_section_1']['#items']['2']['value']); ?></li>
								<?php endif; ?>
								<?php if (!empty($content['field_liste_section_1']['#items']['3']['value'])): ?>
								<li><?php print render($content['field_liste_section_1']['#items']['3']['value']); ?></li>
								<?php endif; ?>
								<?php if (!empty($content['field_liste_section_1']['#items']['4']['value'])): ?>
								<li><?php print render($content['field_liste_section_1']['#items']['4']['value']); ?></li>
								<?php endif; ?>
								<?php if (!empty($content['field_liste_section_1']['#items']['5']['value'])): ?>
								<li><?php print render($content['field_liste_section_1']['#items']['5']['value']); ?></li>
								<?php endif; ?>
							</ul>
							
						<div class="text_folio_2">
								<?php if (!empty($content['field_titre1_projet_tous']['#items']['0']['value'])): ?>
								<h3><?php print render($content['field_titre1_projet_tous']['#items']['0']['value']); ?></h3>
								<?php endif; ?>	
								<?php if (!empty($content['field_txt_projets_tous']['#items']['0']['value'])): ?>
								<p><?php print render($content['field_txt_projets_tous']['#items']['0']['value']); ?></p>
								<?php endif; ?>
								<?php if (!empty($content['field_temoin']['#items']['0']['value'])): ?>
								<p class="temoin"><?php print render($content['field_temoin']['#items']['0']['value']); ?></p>
								<?php endif; ?>
						</div>					  
					</div>
				</section>
				<!-- -------------------------------------------------------------------------- -->				
				<!-- --------------------------  Big Display ---------------------------------- -->	
				<!-- -------------------------------------------------------------------------- -->				
				<section class="projets_all div_equal slider_big_display">
					<div class="side_a equal_this" style="background-image: url('<?php print render($content['field_slider_image']['0']); ?>');background-size: cover; background-position: 50% top;">				

					</div>
					<!-- Deuxième side -->
					<div class="side_b equal_this">
						<?php if (!empty($content['field_titre_section']['#items']['0']['value'])): ?>
						<div class="folio_bg_title">
							<h2><?php print render($content['field_titre_section']['#items']['0']['value']); ?></h2>
							<h4><?php print render($content['field_sous_titre_section_1']['#items']['0']['value']); ?></h4>
						</div>
						<?php endif; ?>
						<div class="text_folio">
							<?php if (!empty($content['field_paragraphe_section_1']['#items']['0']['value'])): ?>
							<p><?php print render($content['field_paragraphe_section_1']['#items']['0']['value']); ?></p>
							<?php endif; ?>	
							<?php if (!empty($content['field_paragraphe_section_2']['#items']['0']['value'])): ?>
							<p><?php print render($content['field_paragraphe_section_2']['#items']['0']['value']); ?></p>
							<?php endif; ?>	
						</div>
						<!-- Liste -->
							<ul class="liste_folio">
								<?php if (!empty($content['field_liste_section_1']['#items']['0']['value'])): ?>
								<li><?php print render($content['field_liste_section_1']['#items']['0']['value']); ?></li>
								<?php endif; ?>
								<?php if (!empty($content['field_liste_section_1']['#items']['1']['value'])): ?>
								<li><?php print render($content['field_liste_section_1']['#items']['1']['value']); ?></li>
								<?php endif; ?>
								<?php if (!empty($content['field_liste_section_1']['#items']['2']['value'])): ?>
								<li><?php print render($content['field_liste_section_1']['#items']['2']['value']); ?></li>
								<?php endif; ?>
								<?php if (!empty($content['field_liste_section_1']['#items']['3']['value'])): ?>
								<li><?php print render($content['field_liste_section_1']['#items']['3']['value']); ?></li>
								<?php endif; ?>
								<?php if (!empty($content['field_liste_section_1']['#items']['4']['value'])): ?>
								<li><?php print render($content['field_liste_section_1']['#items']['4']['value']); ?></li>
								<?php endif; ?>
								<?php if (!empty($content['field_liste_section_1']['#items']['5']['value'])): ?>
								<li><?php print render($content['field_liste_section_1']['#items']['5']['value']); ?></li>
								<?php endif; ?>
							</ul>
							
						<div class="text_folio_2">
							<?php if (!empty($content['field_titre1_projet_tous']['#items']['0']['value'])): ?>
							<h3><?php print render($content['field_titre1_projet_tous']['#items']['0']['value']); ?></h3>
							<?php endif; ?>	
							<?php if (!empty($content['field_txt_projets_tous']['#items']['0']['value'])): ?>
							<p><?php print render($content['field_txt_projets_tous']['#items']['0']['value']); ?></p>
							<?php endif; ?>
							<?php if (!empty($content['field_temoin']['#items']['0']['value'])): ?>
							<p class="temoin"><?php print render($content['field_temoin']['#items']['0']['value']); ?></p>
							<?php endif; ?>
						</div>					  
					</div>
				</section>
			</article>
		</div>
		
	

<script type="text/javascript">  
  /* Fonction qui redimensionne les sections*/
 (function($){$(document).ready(function(){
	$(function() {
        // get test settings
        var byRow = $('body').hasClass('test-rows');
        // apply matchHeight to each item container's items
        $('.div_equal').each(function() {
            $(this).children('.equal_this').matchHeight({
            byRow: byRow
            //property: 'min-height'
            });
        });
    });
});
}(jQuery));
</script>