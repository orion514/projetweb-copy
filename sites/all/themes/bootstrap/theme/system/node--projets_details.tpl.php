<?php
/**
 * @file
 * progress-bar.tpl.php
 *
 * Variables
 * - $percent: The percentage of the progress.
 * - $message: A string containing information to be displayed.
 */
?>	
	  <!-- Affichage Mobile -->
	  <section class="slider slider_small_display">
        <div class="flexslider">
						<ul class="slides">
							<?php if (!empty($content['field_slider_image_mobile']['0']) || !empty($content['field_img_ytb']['0'])): ?>
							<!-- Image1 -->
							<li>
							  <?php if (!empty($content['field_slider_image_mobile']['0'])): ?>
								<a href="<?php print render($content['field_slider_image_mobile']['0']['#item']['alt']); ?>" target="_blank">
								<img src="<?php print render($content['field_slider_image_mobile']['0']); ?>" title="<?php print render($content['field_slider_image_mobile']['0']['#item']['title']); ?>" class="effect_hover">
								</a>
								<?php endif; ?>
								<!-- Vidéo si disponible -->
								<?php if (!empty($content['field_ytb_id']['0'])): ?>			
								<a href="https://youtube.com/embed/<?php print render($content['field_ytb_id']['0']); ?>?width=640&amp;height=360&amp;autoplay=1&amp;vq=hd720;showinfo=1&amp;controls=1&amp;autohide=0&amp;iv_load_policy=0&amp;iframe=1&amp;wmode=opaque" class="colorbox-load" title="<?php print render($content['field_ytb_id']['0']['#item']['title']); ?>">
								<img src="<?php print render($content['field_img_ytb']['0']); ?>" class="effect_hover">
								</a>
								<?php endif; ?>
								<div class="projets_details_slider_text">
									<!-- Titre -->
									<?php if (!empty($content['field_titre_section']['#items']['0']['value'])): ?>
									<h2><?php print render($content['field_titre_section']['#items']['0']['value']); ?></h2>
									<?php endif; ?>
									<!-- Paragraphe-->
									<?php if (!empty($content['field_paragraphe_section_1']['#items']['0']['value'])): ?>
									<p><?php print render($content['field_paragraphe_section_1']['#items']['0']['value']); ?></p>
									<?php endif; ?>
									<!-- Liste -->
										<ul>
											<?php if (!empty($content['field_liste_section_1']['#items']['0']['value'])): ?>
												<?php $list_Numb = $node->field_liste_section_1['und']; ?>
												<?php for ($i = 0; $i < count($list_Numb); ++$i): ?>
													<li><?php print render($content['field_liste_section_1']['#items'][$i]['value']); ?></li>
												<?php endfor; ?>
											<?php endif; ?>
										</ul>
									<?php if (!empty($content['field_projets_logo']['0'])): ?>
									<div class="projets_logo" style="background-image: url('<?php print render($content['field_projets_logo']['0']); ?>');">
									</div>
									<?php endif; ?>
								</div>
			  	    </li>
							<?php endif; ?>
							<!-- Image2 -->
							<?php if (!empty($content['field_slider_image_mobile']['1']) || !empty($content['field_img_ytb']['1'])): ?>
								<li>
										<?php if (!empty($content['field_slider_image_mobile']['1'])): ?>
										<a href="<?php print render($content['field_slider_image_mobile']['1']['#item']['alt']); ?>" target="_blank">
										<img src="<?php print render($content['field_slider_image_mobile']['1']); ?>" title="<?php print render($content['field_slider_image_mobile']['0']['#item']['title']); ?>" class="effect_hover">
										</a>
										<?php endif; ?>
										<!-- Vidéo si disponible -->
										<?php if (!empty($content['field_ytb_id']['1'])): ?>			
										<a href="https://youtube.com/embed/<?php print render($content['field_ytb_id']['1']); ?>?width=640&amp;height=360&amp;vq=hd720;autoplay=1&amp;showinfo=1&amp;controls=1&amp;autohide=0&amp;iv_load_policy=0&amp;iframe=1&amp;wmode=opaque" class="colorbox-load" title="<?php print render($content['field_ytb_id']['1']['#item']['title']); ?>">
										<img src="<?php print render($content['field_img_ytb']['1']); ?>" class="effect_hover">
										</a>
										<?php endif; ?>
										<div class="projets_details_slider_text">
											<!-- Titre -->
											<?php if (!empty($content['field_titre_section']['#items']['1']['value'])): ?>
											<h2><?php print render($content['field_titre_section']['#items']['1']['value']); ?></h2>
											<?php endif; ?>
											<!-- Paragraphe-->
											<?php if (!empty($content['field_paragraphe_section_1']['#items']['1']['value'])): ?>
											<p><?php print render($content['field_paragraphe_section_1']['#items']['1']['value']); ?></p>
											<?php endif; ?>
											<!-- Liste -->
												<ul>
													<?php if (!empty($content['field_liste_section_2']['#items']['0']['value'])): ?>
														<?php $list_Numb2 = $node->field_liste_section_2['und']; ?>
														<?php for ($i = 0; $i < count($list_Numb2); ++$i): ?>
															<li><?php print render($content['field_liste_section_2']['#items'][$i]['value']); ?></li>
														<?php endfor; ?>
													<?php endif; ?>
												</ul>
											<?php if (!empty($content['field_projets_logo']['1'])): ?>
											<div class="projets_logo" style="background-image: url('<?php print render($content['field_projets_logo']['1']); ?>');">
											</div>
											<?php endif; ?>
										</div>
			  	    		</li>
							<?php endif; ?>
							<!-- Image3 -->
							<?php if (!empty($content['field_slider_image_mobile']['2']) || !empty($content['field_img_ytb']['2'])): ?>
								 <li>
										<?php if (!empty($content['field_slider_image_mobile']['2'])): ?>
										<a href="<?php print render($content['field_slider_image_mobile']['2']['#item']['alt']); ?>" target="_blank">
										<img src="<?php print render($content['field_slider_image_mobile']['2']); ?>" title="<?php print render($content['field_slider_image_mobile']['2']['#item']['title']); ?>" class="effect_hover">
										</a>
										<?php endif; ?>
										<!-- Vidéo si disponible -->
										<?php if (!empty($content['field_ytb_id']['2'])): ?>			
										<a href="https://youtube.com/embed/<?php print render($content['field_ytb_id']['2']); ?>?width=640&amp;height=360&amp;vq=hd720;autoplay=1&amp;showinfo=1&amp;controls=1&amp;autohide=0&amp;iv_load_policy=0&amp;iframe=1&amp;wmode=opaque" class="colorbox-load" title="<?php print render($content['field_ytb_id']['1']['#item']['title']); ?>">
										<img src="<?php print render($content['field_img_ytb']['2']); ?>" class="effect_hover">
										</a>
										<?php endif; ?>
										<div class="projets_details_slider_text">
											<!-- Titre -->
											<?php if (!empty($content['field_titre_section']['#items']['2']['value'])): ?>
											<h2><?php print render($content['field_titre_section']['#items']['2']['value']); ?></h2>
											<?php endif; ?>
											<!-- Paragraphe-->
											<?php if (!empty($content['field_paragraphe_section_1']['#items']['2']['value'])): ?>
											<p><?php print render($content['field_paragraphe_section_1']['#items']['2']['value']); ?></p>
											<?php endif; ?>
											<!-- Liste -->
												<ul>
													<?php if (!empty($content['field_liste_section_3']['#items']['0']['value'])): ?>
														<?php $list_Numb3 = $node->field_liste_section_3['und']; ?>
														<?php for ($i = 0; $i < count($list_Numb3); ++$i): ?>
															<li><?php print render($content['field_liste_section_3']['#items'][$i]['value']); ?></li>
														<?php endfor; ?>
													<?php endif; ?>
												</ul>
											<?php if (!empty($content['field_projets_logo']['1'])): ?>
											<div class="projets_logo" style="background-image: url('<?php print render($content['field_projets_logo']['1']); ?>');">
											</div>
											<?php endif; ?>
										</div>
			  	    		</li>
							<?php endif; ?>
							<!-- Image4 -->
							<?php if (!empty($content['field_slider_image_mobile']['3']) || !empty($content['field_img_ytb']['3'])): ?>
								<li>
									<?php if (!empty($content['field_slider_image_mobile']['3'])): ?>
									<a href="<?php print render($content['field_slider_image_mobile']['3']['#item']['alt']); ?>" target="_blank">
									<img src="<?php print render($content['field_slider_image_mobile']['3']); ?>" title="<?php print render($content['field_slider_image_mobile']['3']['#item']['title']); ?>" class="effect_hover">
									</a>
									<?php endif; ?>
									<!-- Vidéo si disponible -->
									<?php if (!empty($content['field_ytb_id']['3'])): ?>			
									<a href="https://youtube.com/embed/<?php print render($content['field_ytb_id']['3']); ?>?width=640&amp;height=360&amp;vq=hd720;autoplay=1&amp;showinfo=1&amp;controls=1&amp;autohide=0&amp;iv_load_policy=0&amp;iframe=1&amp;wmode=opaque" class="colorbox-load" title="<?php print render($content['field_ytb_id']['1']['#item']['title']); ?>">
									<img src="<?php print render($content['field_img_ytb']['3']); ?>" class="effect_hover">
									</a>
									<?php endif; ?>
									<div class="projets_details_slider_text">
										<!-- Titre -->
										<?php if (!empty($content['field_titre_section']['#items']['3']['value'])): ?>
										<h2><?php print render($content['field_titre_section']['#items']['3']['value']); ?></h2>
										<?php endif; ?>
										<!-- Paragraphe-->
										<?php if (!empty($content['field_paragraphe_section_1']['#items']['3']['value'])): ?>
										<p><?php print render($content['field_paragraphe_section_1']['#items']['3']['value']); ?></p>
										<?php endif; ?>
										<!-- Liste -->
											<ul>
												<?php if (!empty($content['field_liste_section_4']['#items']['0']['value'])): ?>
													<?php $list_Numb4 = $node->field_liste_section_4['und']; ?>
													<?php for ($i = 0; $i < count($list_Numb4); ++$i): ?>
														<li><?php print render($content['field_liste_section_4']['#items'][$i]['value']); ?></li>
													<?php endfor; ?>
												<?php endif; ?>
											</ul>
										<?php if (!empty($content['field_projets_logo']['1'])): ?>
										<div class="projets_logo" style="background-image: url('<?php print render($content['field_projets_logo']['1']); ?>');">
										</div>
										<?php endif; ?>
									</div>
			  	    	</li>
							<?php endif; ?>
			      </ul> 
        </div>   
	  </section>
	  <!-- Fin de l'affichage mobile -->








		<div class="responsive-front">
			<article>
				<section class="projets_details_1 div_equal">
					<div class="side_a equal_this">	
						<!-- Image en lien -->						
						<?php if (!empty($content['field_img_section_1']['0'])): ?>
						<a href="<?php print render($content['field_img_section_1']['0']['#item']['alt']); ?>" target="_blank" title="<?php print render($content['field_img_section_1']['0']['#item']['title']); ?>">	
						<img src="<?php print render($content['field_img_section_1']['0']); ?>" class="img_projets_all_banner_Big">
						</a>
						<?php endif; ?>	
						<!-- Vidéo si disponible -->
						<?php if (!empty($content['field_ytb_id']['0'])): ?>			
						<a href="https://youtube.com/embed/<?php print render($content['field_ytb_id']['0']); ?>?width=640&amp;height=360&amp;autoplay=1&amp;showinfo=1&amp;controls=1&amp;autohide=0&amp;iv_load_policy=0&amp;iframe=1&amp;wmode=opaque&vq=hd720" class="colorbox-load" title="<?php print render($content['field_ytb_id']['0']['#item']['title']); ?>">
						<img src="<?php print render($content['field_img_ytb']['0']); ?>" class="img_projets_all_banner_Big">
						</a>
						<?php endif; ?>
					</div>
					<div class="side_b equal_this">					
						<!-- Zone de textes -->
						<div class="text_projets_details">
								<!-- Titre -->
								<?php if (!empty($content['field_titre_section']['#items']['0']['value'])): ?>
								<h2><?php print render($content['field_titre_section']['#items']['0']['value']); ?></h2>
								<?php endif; ?>
								<!-- Paragraphe-->
								<?php if (!empty($content['field_paragraphe_section_1']['#items']['0']['value'])): ?>
								<p><?php print render($content['field_paragraphe_section_1']['#items']['0']['value']); ?></p>
								<?php endif; ?>
								<!-- Liste -->
								<ul>
									<?php if (!empty($content['field_liste_section_1']['#items']['0']['value'])): ?>
										<?php $list_Numb = $node->field_liste_section_1['und']; ?>
										<?php for ($i = 0; $i < count($list_Numb); ++$i): ?>
											<li><?php print render($content['field_liste_section_1']['#items'][$i]['value']); ?></li>
										<?php endfor; ?>
									<?php endif; ?>
								</ul>
								<?php if (!empty($content['field_projets_logo']['0'])): ?>
								<div class="projets_logo" style="background-image: url('<?php print render($content['field_projets_logo']['0']); ?>');">
								</div>
								<?php endif; ?>
						</div>
					</div>
				</section>

				<section class="projets_details_1 div_equal">
					<div class="side_a equal_this">	
						<!-- Image en lien -->						
						<?php if (!empty($content['field_img_section_1']['1'])): ?>
						<a href="<?php print render($content['field_img_section_1']['1']['#item']['alt']); ?>" target="_blank" title="<?php print render($content['field_img_section_1']['1']['#item']['title']); ?>">	
						<img src="<?php print render($content['field_img_section_1']['1']); ?>" class="img_projets_all_banner_Big">
						<?php endif; ?>	
						</a>
						<!-- Vidéo si disponible -->
						<?php if (!empty($content['field_ytb_id']['1'])): ?>			
						<a href="https://youtube.com/embed/<?php print render($content['field_ytb_id']['1']); ?>?width=640&amp;height=360&amp;vq=hd720;autoplay=1&amp;showinfo=1&amp;controls=1&amp;autohide=0&amp;iv_load_policy=0&amp;iframe=1&amp;wmode=opaque" class="colorbox-load" title="<?php print render($content['field_ytb_id']['1']['#item']['title']); ?>">
						<img src="<?php print render($content['field_img_ytb']['1']); ?>" class="img_projets_all_banner_Big">
						</a>
						<?php endif; ?>
					</div>
					<div class="side_b equal_this">					
						<!-- Zone de textes -->
						<div class="text_projets_details">
								<!-- Titre -->
								<?php if (!empty($content['field_titre_section']['#items']['1']['value'])): ?>
								<h2><?php print render($content['field_titre_section']['#items']['1']['value']); ?></h2>
								<?php endif; ?>
								<!-- Paragraphe-->
								<?php if (!empty($content['field_paragraphe_section_1']['#items']['1']['value'])): ?>
								<p><?php print render($content['field_paragraphe_section_1']['#items']['1']['value']); ?></p>
								<?php endif; ?>
								<!-- Liste -->
								<ul>
									<?php if (!empty($content['field_liste_section_2']['#items']['0']['value'])): ?>
										<?php $list_Numb2 = $node->field_liste_section_2['und']; ?>
										<?php for ($i = 0; $i < count($list_Numb2); ++$i): ?>
											<li><?php print render($content['field_liste_section_2']['#items'][$i]['value']); ?></li>
										<?php endfor; ?>
									<?php endif; ?>
								</ul>
								<?php if (!empty($content['field_projets_logo']['1'])): ?>
								<div class="projets_logo" style="background-image: url('<?php print render($content['field_projets_logo']['1']); ?>');">
								</div>
								<?php endif; ?>	
					</div>
				</section>

				<?php if (!empty($content['field_titre_section']['#items']['2']['value'])): ?>
				<section class="projets_details_1 div_equal">
					<div class="side_a equal_this">	
						<!-- Image en lien -->						
						<?php if (!empty($content['field_img_section_1']['2'])): ?>
						<a href="<?php print render($content['field_img_section_1']['2']['#item']['alt']); ?>" target="_blank" title="<?php print render($content['field_img_section_1']['2']['#item']['title']); ?>">	
						<img src="<?php print render($content['field_img_section_1']['2']); ?>" class="img_projets_all_banner_Big">
						<?php endif; ?>	
						</a>
						<!-- Vidéo si disponible -->
						<?php if (!empty($content['field_ytb_id']['2'])): ?>			
						<a href="https://youtube.com/embed/<?php print render($content['field_ytb_id']['2']); ?>?width=640&amp;height=360&amp;vq=hd720;autoplay=1&amp;showinfo=1&amp;controls=1&amp;autohide=0&amp;iv_load_policy=0&amp;iframe=1&amp;wmode=opaque" class="colorbox-load" title="<?php print render($content['field_ytb_id']['1']['#item']['title']); ?>">
						<img src="<?php print render($content['field_img_ytb']['2']); ?>" class="img_projets_all_banner_Big">
						</a>
						<?php endif; ?>
					</div>
					<div class="side_b equal_this">					
						<!-- Zone de textes -->
						<div class="text_projets_details">
								<!-- Titre -->
								<?php if (!empty($content['field_titre_section']['#items']['2']['value'])): ?>
								<h2><?php print render($content['field_titre_section']['#items']['2']['value']); ?></h2>
								<?php endif; ?>
								<!-- Paragraphe-->
								<?php if (!empty($content['field_paragraphe_section_1']['#items']['2']['value'])): ?>
								<p><?php print render($content['field_paragraphe_section_1']['#items']['2']['value']); ?></p>
								<?php endif; ?>
								<!-- Liste -->
								<ul>
									<?php if (!empty($content['field_liste_section_3']['#items']['0']['value'])): ?>
										<?php $list_Numb3 = $node->field_liste_section_3['und']; ?>
										<?php for ($i = 0; $i < count($list_Numb3); ++$i): ?>
											<li><?php print render($content['field_liste_section_3']['#items'][$i]['value']); ?></li>
										<?php endfor; ?>
									<?php endif; ?>
								</ul>
								<?php if (!empty($content['field_projets_logo']['1'])): ?>
								<div class="projets_logo" style="background-image: url('<?php print render($content['field_projets_logo']['1']); ?>');">
								</div>
								<?php endif; ?>	
					</div>
				</section>
				<?php endif; ?>

				<?php if (!empty($content['field_titre_section']['#items']['3']['value'])): ?>
				<section class="projets_details_1 div_equal">
					<div class="side_a equal_this">	
						<!-- Image en lien -->						
						<?php if (!empty($content['field_img_section_1']['3'])): ?>
						<a href="<?php print render($content['field_img_section_1']['3']['#item']['alt']); ?>" target="_blank" title="<?php print render($content['field_img_section_1']['3']['#item']['title']); ?>">	
						<img src="<?php print render($content['field_img_section_1']['3']); ?>" class="img_projets_all_banner_Big">
						<?php endif; ?>	
						</a>
						<!-- Vidéo si disponible -->
						<?php if (!empty($content['field_ytb_id']['3'])): ?>			
						<a href="https://youtube.com/embed/<?php print render($content['field_ytb_id']['3']); ?>?width=640&amp;height=360&amp;vq=hd720;autoplay=1&amp;showinfo=1&amp;controls=1&amp;autohide=0&amp;iv_load_policy=0&amp;iframe=1&amp;wmode=opaque" class="colorbox-load" title="<?php print render($content['field_ytb_id']['3']['#item']['title']); ?>">
						<img src="<?php print render($content['field_img_ytb']['3']); ?>" class="img_projets_all_banner_Big">
						</a>
						<?php endif; ?>
					</div>
					<div class="side_b equal_this">					
						<!-- Zone de textes -->
						<div class="text_projets_details">
								<!-- Titre -->
								<?php if (!empty($content['field_titre_section']['#items']['3']['value'])): ?>
								<h2><?php print render($content['field_titre_section']['#items']['3']['value']); ?></h2>
								<?php endif; ?>
								<!-- Paragraphe-->
								<?php if (!empty($content['field_paragraphe_section_1']['#items']['3']['value'])): ?>
								<p><?php print render($content['field_paragraphe_section_1']['#items']['3']['value']); ?></p>
								<?php endif; ?>
								<!-- Liste -->
								<ul>
									<?php if (!empty($content['field_liste_section_4']['#items']['0']['value'])): ?>
										<?php $list_Numb4 = $node->field_liste_section_4['und']; ?>
										<?php for ($i = 0; $i < count($list_Numb4); ++$i): ?>
											<li><?php print render($content['field_liste_section_4']['#items'][$i]['value']); ?></li>
										<?php endfor; ?>
									<?php endif; ?>
								</ul>
								<?php if (!empty($content['field_projets_logo']['1'])): ?>
								<div class="projets_logo" style="background-image: url('<?php print render($content['field_projets_logo']['1']); ?>');">
								</div>
								<?php endif; ?>	
					</div>
				</section>
				<?php endif; ?>
			</article>
		</div>
		
	

<script type="text/javascript">  
  /* Fonction qui redimensionne les sections*/
 (function($){$(document).ready(function(){
	$(function() {
        // get test settings
        var byRow = $('body').hasClass('test-rows');
        // apply matchHeight to each item container's items
        $('.div_equal').each(function() {
            $(this).children('.equal_this').matchHeight({
            byRow: byRow
            //property: 'min-height'
            });
        });
    });
});
}(jQuery));


 (function($){$(document).ready(function(){
    $(window).load(function(){
      $('.flexslider').flexslider({
        animation: "slide"
      });
    });
});
}(jQuery));
</script>