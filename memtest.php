<?php
 $step = 1;
 while(TRUE) {
  $chunk = str_repeat('0123456789', 128*1024*$step++);
  print 'Memory usage: '. round(memory_get_usage()/(1024*1024)) . 'M<br />';
  unset($chunk);
 }
 ?>